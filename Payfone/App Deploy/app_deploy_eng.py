# -*- coding: utf-8 -*-

# Initial version by Nicholas Sawlidi dated 21MAR14
# 26MAR14 Updated with get_APP function, a direct conversion 
#   of a script written by Greg Machacek
# Updated 24JUL by NS with the new APP04 and a change to update new configs
# Updated 26AUG by NS with new production servers and environments 


from fabric.api import run, sudo, env, quiet, local, parallel, warn_only
from fabric.colors import red, cyan
from fabric.decorators import runs_once
from generic import get_user, rPrint
import re, smtplib, sys
from email.mime.text import MIMEText
from time import sleep, time
from socket import gethostname

APP_pattern = re.compile("APP")
ice_pattern = re.compile("ice")

f5_env = "TEST-1"
environ = "TEST-1"

get_user()


#{{{ APP QA ENV Set
def APPQA():
    """ QA  """
    env.hosts = ['']
    global f5_env
    global environ
    f5_env = "QA"
    environ = "QA"
#}}}

#{{{ APP Upgrade - no restart
def APP_upgrade(version='0.0.0', config_update="no"):
    """ Just install APP. No restart """
    send_predeploy_email(version) 
    rPrint(cyan, 'Stopping APP')
    with quiet():
        APP_stop()
    rPrint(cyan, "Installing APP")
    get_APP(version, config_update)
    rPrint(cyan, "Installation complete!")
#}}}

#{{{ APP Stop Function
def APP_stop():
    """ Stop the APP Processes """
    rPrint(cyan, "Stopping APP")
    with quiet():
        sudo('ps -ef |grep -v grep |grep Controller |awk \'{print $2}\' |xargs /bin/kill -9 ')
        sleep(1)
        rPrint(cyan, 'Controller stopped')
        rPrint(cyan, 'Stopping the rest of the APP processes')
        sudo('ps -ef |grep -v grep |grep APP |awk \'{print $2}\' |xargs /bin/kill -9 ')
        sleep(1)
        rPrint(cyan, "APP has been stopped")
        
#}}}

#{{{ APP Start Function
def APP_start(version='0.0.0'):
    """ Start the APP process """
    rPrint(cyan, "Starting APP")
    with quiet():
        sudo('/sbin/service APP start')
        rPrint(cyan, 'APP is starting up. Please wait 2-5 minutes depending on the environment')
        if version == '0.0.0':
            send_restart_email()
        else:
            send_deploy_email(version) 
#}}}

#{{{ APP Restart Function
@parallel(pool_size=4)
def APP_restart(activemq_stop="no"):
    """ Stop the APP and ActiveMQ Processes """
    APP_stop()
    if activemq_stop == "yes":
        rPrint(cyan, "Restarting ActiveMQ")
        with quiet():
            sudo('/sbin/service activemq restart')
            rPrint(cyan, "ActiveMQ restarted, sleeping for 60 seconds before starting APP.")
            sleep(60)
    APP_start()
#}}}

#{{{ Send Predeploy Email
@runs_once
def send_predeploy_email(version='0.0.0'):
    """Send a predeploy email - send_deploy_email:<VERSION>"""
    rPrint(cyan, 'Sending predeploy email')
    from_addr = 'APP-dev@payfone.com'
    to_addr = ['APP-dev@payfone.com', 'qa@payfone.com']
    msg = 'Greetings,\n\n\nAPP build %s will be deployed by %s to %s. The environment will be down until the deploy is complete. Please do not test until this process is complete.\n\n\nThanks,\n\n-APP Dev' % (version,env.user,environ)
    message = MIMEText(msg)
    message['Subject'] = 'APP build %s deployment to %s' % (version,environ)
    message['From'] = from_addr
    message['To'] = ",".join(to_addr)
    server = smtplib.SMTP('')
    server.sendmail(from_addr, to_addr, message.as_string())
    server.quit()
    rPrint(cyan, 'Predeploy email sent!')
#}}}

#{{{ Send Post Deploy Email
@runs_once
def send_deploy_email(version='0.0.0'):
    """Send a deploy email - send_deploy_email:<VERSION>,<ENVIRONMENT>"""
    rPrint(cyan, 'Sending post deploy email')
    from_addr = 'APP-dev@payfone.com'
    to_addr = ['APP-dev@payfone.com', 'qa@payfone.com']    
    msg = 'Greetings,\n\n\nAPP build %s has been deployed by %s to %s and APP has been restarted. Please wait two to five minutes for all processes to resume normal functionality.\n\n\nThanks,\n\n-APP Dev' % (version,env.user,environ)
    message = MIMEText(msg)
    message['Subject'] = 'APP build %s deployment to %s' % (version,environ)
    message['From'] = from_addr
    message['To'] = ",".join(to_addr)
    server = smtplib.SMTP('')
    server.sendmail(from_addr, to_addr, message.as_string())
    server.quit()
    rPrint(cyan, 'Post deploy email sent!')
#}}}

#{{{ Send Restart Email
@runs_once
def send_restart_email():
    """Send a deploy email - send_deploy_email:<VERSION>,<ENVIRONMENT>"""
    rPrint(cyan, 'Sending post deploy email')
    from_addr = 'APP-dev@payfone.com'
    to_addr = ['APP-dev@payfone.com', 'qa@payfone.com']
    msg = 'Greetings,\n\n\nAPP in %s has been restarted by %s. Please wait two to five minutes for all processes to resume normal functionality.\n\n\nThanks,\n\n-APP Dev' % (environ,env.user)
    message = MIMEText(msg)
    message['Subject'] = 'APP restart on %s' % environ
    message['From'] = from_addr
    message['To'] = ",".join(to_addr)
    server = smtplib.SMTP('')
    server.sendmail(from_addr, to_addr, message.as_string())
    server.quit()
    rPrint(cyan, 'Post deploy email sent!')
#}}}

#{{{ Get APP conversion
def get_APP(version='0.0.0', config_update="no"):
    """ Get the APP tarball, untar it and symlink it """
    APP_tar = "APP-" + version + ".tar"
    APP_tar_re = re.compile(APP_tar)
    with quiet():
        # Check for the Directory
        rPrint(cyan,"Verifying that the APP directory exists.")
        dir_exists = False
        o_dir_list = sudo('ls -l /opt/payfone/.')
        dir_list = o_dir_list.splitlines()
        for pf_dir in dir_list:
            dir_match = re.search(APP_pattern, pf_dir)
            if dir_match:
                dir_exists = True
        if not dir_exists:
            sudo('mkdir -p /opt/payfone/APP')
        rPrint(cyan, "Checking if APP %s tarball exists" % version)
        version_pattern = re.compile(version)
        tarball_exists = False
        listing = sudo("ssh -i /opt/scripts/deploy_rsa deployment@dendev01.payfone.com ls -l /var/www/html/APP")
        listing = listing.splitlines()
        for item in listing:
            version_match = re.search(version_pattern, item)
            if version_match:
                tarball_exists = True
        if tarball_exists:
            rPrint(cyan,"Checking to see if %s has already been downloaded" % APP_tar)
            tar_check = False
            o_APP_home_dir = sudo('ls -l /home/APP/')
            APP_home_dir = o_APP_home_dir.splitlines()
            for tarball in APP_home_dir:
                t_match = re.search(APP_tar_re, tarball)
                if t_match:
                    tar_check = True
            if not tar_check:
                rPrint(cyan, "APP %s tarball exists and has not been downloaded. Downloading %s" % (version,APP_tar))
                sudo("scp -i /opt/scripts/deploy_rsa deployment@dendev01.payfone.com:/var/www/html/APP/%s /home/APP/." % APP_tar)
                sudo("chown APP: /home/APP/%s" % APP_tar)
                rPrint(cyan, "Unpacking %s" % APP_tar)
                sudo("mkdir -p /opt/payfone/APP/%s" % version)
                sudo("tar xvf /home/APP/%s -C /opt/payfone/APP/%s/." % (APP_tar,version))
                rPrint(cyan,"Unlinking and relinking the symlinks.")
                sudo("unlink /opt/payfone/APP/bin")
                sudo("unlink /opt/payfone/APP/lib")
                sudo("ln -s /opt/payfone/APP/%s/bin /opt/payfone/APP/bin" % version)
                sudo("ln -s /opt/payfone/APP/%s/lib /opt/payfone/APP/lib" % version)
                rPrint(cyan,"Ensuring permissions are correct.")
                sudo("chown -R APP: /opt/payfone/APP/%s" % version)
            else:
                rPrint(cyan, "Tarball already downloaded, proceeding to unpack %s" % APP_tar)
                sudo("mkdir -p /opt/payfone/APP/%s" % version)
                sudo("tar xvf /home/APP/%s -C /opt/payfone/APP/%s/." % (APP_tar,version))
                rPrint(cyan,"Unlinking and relinking the symlinks.")
                sudo("unlink /opt/payfone/APP/bin")
                sudo("unlink /opt/payfone/APP/lib")
                sudo("ln -s /opt/payfone/APP/%s/bin /opt/payfone/APP/bin" % version)
                sudo("ln -s /opt/payfone/APP/%s/lib /opt/payfone/APP/lib" % version)
                rPrint(cyan,"Ensuring permissions are correct.")
                sudo("chown -R APP: /opt/payfone/APP/%s" % version)
        else:
            rPrint(red, "APP version %s tarball not found on deployment server. Exiting." % version)
            sys.exit()
        if config_update == "yes":
            rPrint(cyan, "Copying configs")
            sudo('cp -p /opt/payfone/APP/new/* /opt/payfone/APP/config/.')
        rPrint(cyan, "Installation of APP %s is complete!" % version)
#}}}

#{{{ APP Full Deploy with Restart
def APP_deploy(version='0.0.0', config_update="no"):
    """ Do a full APP Deploy. Will install and restart APP """
    send_predeploy_email(version) 
    rPrint(cyan, 'Stopping APP')
    APP_stop()
    rPrint(cyan, "Installing APP")
    get_APP(version, config_update)
    rPrint(cyan, "Installation complete!")
    rPrint(cyan, "Starting APP.")
    APP_start(version)
    rPrint(cyan, "APP %s deployment complete!" % version)
#}}}
