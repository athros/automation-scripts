#!/opt/python2.7/bin/python2.7

from fabric.operations import local, get
from fabric.api import env, quiet
from bs4 import BeautifulSoup
from datetime import date
from email.mime.text import MIMEText
from netaddr import *
import re, xmlrpclib, smtplib

env.warn_only = True

DEBUG = False

TODAY = date.today()
YESTERDAY = date(date.today().year, date.today().month, date.today().day - 1)

CIDR_NETMASK = {'4' : '240.0.0.0',
                '5' : '248.0.0.0',
                '6' : '252.0.0.0',
                '7' : '254.0.0.0',
                '8' : '255.0.0.0',
                '9' : '255.128.0.0',
                '10' : '255.192.0.0',
                '11' : '255.224.0.0',
                '12' : '255.240.0.0',
                '13' : '255.248.0.0',
                '14' : '255.252.0.0',
                '15' : '255.254.0.0',
                '16' : '255.255.0.0',
                '17' : '255.255.128.0',
                '18' : '255.255.192.0',
                '19' : '255.255.224.0',
                '20' : '255.255.240.0',
                '21' : '255.255.248.0',
                '22' : '255.255.252.0',
                '23' : '255.255.254.0',
                '24' : '255.255.255.0',
                '25' : '255.255.255.128',
                '26' : '255.255.255.192',
                '27' : '255.255.255.224',
                '28' : '255.255.255.240',
                '29' : '255.255.255.248',
                '30' : '255.255.255.252',
                '32' : '255.255.255.255'}

EMAIL_BODY = "Greetings,\n\nHere is a list of the changes made to the Juniper Whitelist:\n\n"

#Begin Pentaho functions

def pull_file():
    """Fabric/expect pull of the file"""
    env.user = ''
    env.password = ''
    env.host_string = '<SERVER>'
    filename = '<FILE>'
    remote_path = '~'
    local_path = '~'
    try:
        local('mv' % (filename,filename,YESTERDAY))
    except:
        pass
    get(remote_path, local_path)

def get_whitelist():
    """Overall function for collecting the Whitelist and changes"""
    pen_whitelist = {}
    current_filename = ('<FILE>')
    current_file_dict = parse_file(current_filename)
    yesterday_filename = ('<OLD_FILE>' % str(YESTERDAY))
    try:
        yesterday_pen_dict = parse_file(yesterday_filename)
        pen_whitelist.update(diff_files(current_file_dict, yesterday_pen_dict))
    except:
        for item in current_file_dict:
            add_item = "+" + str(item)
            whitelist[add_item] = current_file_dict[item]
    return whitelist

def parse_file(filename):
    """Takes in a .csv and parses it into a dictionary"""
    ip_dict = {}
    comment_start = "Provided by "
    cidr_pattern = re.compile('/')
    range_pattern = re.compile('-')
    try:
        pen_file = open(filename)
    except:
        return "No file found"
    for line in pen_file:
        cidr_match = re.search(cidr_pattern, line)
        range_match = re.search(range_pattern, line)
        comment_split = line.split(",")[1].rstrip()
        comment = comment_start + comment_split
        if cidr_match:
            ip_addr = line.split(",")[0]
            ip_dict[ip_addr] = comment
        elif range_match:
            ip_split = line.split(",")[0]
            first_ip = ip_split.split("-")[0]
            last_ip = ip_split.split("-")[1]
            range_dict = expand_range(first_ip, last_ip, comment)
            ip_dict.update(range_dict)
        else:
            ip_single = line.split(",")[0] + "/32"
            ip_dict[ip_single] = comment
    return ip_dict

def seperate_mno_ip(full_dict):
    MNO_2 = {}
    MNO_1 = {}
    MNO_3 = {}
    MNO_4 = {}
    MNO_2_IP_ADDRS = {}
    MNO_1_IP_ADDRS = {} 
    MNO_3_IP_ADDRS = {}
    MNO_4_IP_ADDRS = {}
    MNO_1_pattern = re.compile('MNO_1')
    MNO_2_pattern = re.compile('MNO_2')
    MNO_3_pattern = re.compile('MNO_3')
    MNO_4_pattern = re.compile('MNO_4')
    for item in full_dict:
        MNO_1_match = re.search(MNO_1_pattern, full_dict[item])
        MNO_2_match = re.search(MNO_2_pattern, full_dict[item])
        MNO_3_match = re.search(MNO_3_pattern, full_dict[item])
        MNO_4_match = re.search(MNO_4_pattern, full_dict[item])
        if MNO_1_match:
            MNO_1[item] = full_dict[item]
        elif MNO_2_match:
            MNO_2[item] = full_dict[item]
        elif MNO_3_match:
            MNO_3[item] = full_dict[item]
        elif MNO_4_match:
            MNO_4[item] = full_dict[item]
    MNO_2_IP_ADDRS = condense_ranges(MNO_2)
    MNO_1_IP_ADDRS = condense_ranges(MNO_1)
    MNO_3_IP_ADDRS = condense_ranges(MNO_3)
    MNO_4_IP_ADDRS = condense_ranges(MNO_4)
    full_list = {}
    full_list.update(MNO_2_IP_ADDRS)
    full_list.update(MNO_1_IP_ADDRS)
    full_list.update(MNO_3_IP_ADDRS)
    full_list.update(MNO_4_IP_ADDRS)
    return full_list

def condense_ranges(mno_dict):
    cidr_pattern = re.compile('CIDR')
    net_pattern = re.compile('NET')
    add_pattern = re.compile("\+")
    remove_pattern = re.compile("-")
    eight_pattern = re.compile("\/8")
    condensed_dict = {}
    class_a = []
    comment = ""
    for item in mno_dict:
        if comment != "":
            pass
        else:
            comment = mno_dict[item]
        whois_cidr = ""
        net_cidr = []
        ip_addr = IPNetwork(item)
        whois_info = []
        with quiet():
            whois_info = local('whois %s' % str(ip_addr), capture=True).splitlines()
        for line in whois_info:
            cidr_match = re.search(cidr_pattern, line)
            if cidr_match:
                cidr_line = item.split(" ")
                whois_cidr = "+" + str(cidr_line[len(cidr_line)-1])
            elif not cidr_match:
                net_match = re.search(net_pattern, line)
                if net_match:
                    net_cidr.append(str(line))
            else:
                print "ERROR NO CIDR LINE IN WHOIS QUERY"
        if len(net_cidr) > 0:
            for line in net_cidr:
                network_line = line.split(" ")
                start_range = network_line[len(network_line)-3]
                end_range = network_line[len(network_line)-1]
                ip_range = IPRange(start_range, end_range)
                for item in ip_range.cidrs():
                    eight_match = re.search(eight_pattern, str(item))
                    if eight_match:
                        pass
                    else:
                        whois_cidr = "+" + str(item)
        if len(class_a) > 0:
            if whois_cidr in class_a:
                pass
            else:
                for item in class_a:
                    temp_item = ""
                    temp_whois = ""
                    add_match = re.search(add_pattern, item)
                    remove_match = re.search(remove_pattern, item)
                    whois_add_match = re.search(add_pattern, whois_cidr)
                    whois_remove_match = re.search(remove_pattern, whois_cidr)
                    if add_match:
                        temp_item = item.lstrip("+")
                    elif remove_match:
                        temp_item = item.lstrip("-")
                    if whois_add_match:
                        temp_whois = whois_cidr.lstrip("+")
                    elif whois_remove_match:
                        temp_whois = whois_cidr.lstrip("-")
                    if IPNetwork(temp_whois) in IPNetwork(temp_item):
                        pass
                    else:
                        class_a.append(whois_cidr)
                        class_a = list(set(class_a))
        else:
            class_a.append(whois_cidr)
    class_a = list(set(class_a))
    for item in class_a:
        condensed_dict[item] = comment
    return condensed_dict

def diff_files(current_dict, old_dict):
    """Compare old dict and new dict, pegging items to add (+) or remove (-) based on pentaho"""
    diff_dict = {}
    for item in old_dict:
        if item in current_dict:
            pass
        else:
            rem_item = "-" + str(item)
            diff_dict[rem_item] = old_dict[item]
    for item in current_dict:
        if item in old_dict:
            pass
        else:
            add_item = "+" + str(item)
            diff_dict[add_item] = current_dict[item]
    return diff_dict

def expand_range(beginning, end, comment):
    """App has IP's listed as xxx.xxx.xxx.yyy-xxx.xxx.xxx.zzz
                This will expand those ranges into individual /32's
                and then return the expanded dict"""
    expanded_dict = {}
    start_range = beginning.split(".")[3]
    end_range = int(end.split(".")[3])
    fourth_octet = int(start_range)
    while fourth_octet <= end_range:
        ip_split = re.split(r'(\.|/)', beginning)
        ip_split[len(ip_split)-1] = str(fourth_octet)
        ip_addr = "".join(ip_split) + "/32"
        expanded_dict[ip_addr] = comment
        fourth_octet += 1
    return expanded_dict

#End Pentaho Functions

#Begin Juniper Functions

def get_juniper_whitelist():
    members_pattern = re.compile('Members')
    local('/bin/bash get_juniper_whitelist.sh')
    whitelist_file = open("juniper_whitelist.tmp")
    juniper_list = []
    for line in whitelist_file:
        members_match = re.search(members_pattern, line)
        if members_match:
            juniper_list.append(line)
    whitelist_file.close()
    local('rm -f juniper_whitelist.tmp')
    if len(juniper_list) > 0:
        whitelist_members = juniper_list[0]
        whitelist_members = whitelist_members.lstrip().rstrip()
        whitelist_members = whitelist_members.split(" ")
        whitelist_members.pop(0)
        initial_juniper_list = []
        for item in whitelist_members:
            item.lstrip("\"").rstrip("\"")
            initial_juniper_list.append(item)
        return initial_juniper_list
    else:
        initial_juniper_list = []
        return initial_juniper_list

def diff_juni_pen(juniper_whitelist, whitelist):
    add_pattern = re.compile("\+")
    remove_pattern = re.compile("-")
    final_whitelist = {}
    for item in pentaho_whitelist:
        add_match = re.search(add_pattern, item)
        remove_match = re.search(remove_pattern, item)
        if add_match:
            temp_item = item.lstrip("+")
            if temp_item in juniper_whitelist:
                pass
            else:
                final_whitelist[item] = pentaho_whitelist[item]
        elif remove_match:
            temp_item = item.lstrip("-")
            if temp_item in juniper_whitelist:
                final_whitelist[item] = pentaho_whitelist[item]
            else:
                pass
    return final_whitelist

def upload_whitelist_to_juniper(whitelist, juniper_full_whitelist):
    global EMAIL_BODY
    add_pattern = re.compile("\+")
    remove_pattern = re.compile("-")
    remove_entry = {}
    add_entry = {}
    if len(whitelist) > 0:
        for item in whitelist:
            add_match = re.search(add_pattern, item)
            remove_match = re.search(remove_pattern, item)
            if add_match:
                EMAIL_BODY += "Added Entry: " + str(item.lstrip("+")) + " - " + whitelist[item] + "\n"
                add_entry[item.lstrip("+")] = whitelist[item]
            elif remove_match:
                EMAIL_BODY += "Removed Entry: " + str(item.lstrip("-")) + " - " + whitelist[item] + "\n"
                remove_entry[item.lstrip("-")] = whitelist[item]
        if len(remove_entry) > 0 and len(juniper_full_whitelist) > 0:
            for item in remove_entry:
                juniper_full_whitelist.delete(item)
            add_entry.update(juniper_full_whitelist)
            file = open('whitelist_commands.txt', 'w')
            file.write('unset group address untrust device_whitelist_http clear\n\n')
            for item in remove_entry:
                file.write('unset address untrust %s\n\n' % item)
            file.write('save\n\n')
            for item in whitelist:
                name = item
                ip_address = item.split("/")[0]
                netmask = CIDR_NETMASK[item.split("/")[1]]
                comment = whitelist[item]
                file.write('set address untrust %s %s %s "%s"\n\n' % (name,ip_address,netmask,comment))
                file.write('set group address untrust device_whitelist_http add "%s"\n\n' % name)
            file.write('save\n\nexit\n\n')
            file.close()
            local('/bin/bash upload_whitelist.sh')
            local('rm -f whitelist_commands.txt')
        else:
            file = open('whitelist_commands.txt', 'w')
            for item in whitelist:
                name = item.lstrip("+")
                ip_address = item.split("/")[0].lstrip("+")
                netmask = CIDR_NETMASK[item.split("/")[1]]
                comment = whitelist[item]
                file.write('set address untrust %s %s %s "%s"\n\n' % (name,ip_address,netmask,comment))
                file.write('set group address untrust device_whitelist_http add "%s"\n\n' % name)
            file.write('save\n\nexit\n\n')
            file.close()
            local('/bin/bash upload_whitelist.sh')
            local('rm -f whitelist_commands.txt')
    else:
        EMAIL_BODY += "No changes at this time!"

def get_deny_log_whitelist():
    #Locate the file with try: except:
    #parse through the file. Use RE removals if necessary
    #load into global DENY_LOG_WHITELIST
    pass

#End Juniper Functions

def send_email():
    from_addr = ''
    to_addr = ''
    msg = '%s\n\nThanks,\n\n-The Operations Team' % EMAIL_BODY
    message = MIMEText(msg)
    message['Subject'] = 'Juniper Whitelist Update'
    message['From'] = from_addr
    message['To'] = to_addr
    if DEBUG:
        filename = 'whitelist_upload_output.tmp'
        f = file(filename)
        MNO_2achment = MIMEText(f.read())
        MNO_2achment.add_header('Content-Disposition', 'MNO_2achment', filename=filename)
        message.MNO_2ach(MNO_2achment)
    local('rm -f whitelist_upload_output.tmp')
    server = smtplib.SMTP('<MAIL_SERVER:25')
    server.sendmail(from_addr, to_addr, message.as_string())
    server.quit()

if __name__ == "__main__":
    print "Pulling today's list"
    pull_file()
    print "Parsing today's file and Yesterdays file."
    pentaho_whitelist = get_whitelist()
    print "Seperating out IP's and condensing the ranges"
    final_whitelist = seperate_mno_ip(whitelist)
    print "Getting the existing Juniper whitelist"
    juniper_whitelist = get_juniper_whitelist()
    if len(juniper_whitelist) > 0:
        print "Juniper whitelist has existing members. Diffing the list against the pentaho list"
        final_whitelist = diff_juni_pen(juniper_whitelist, final_pentaho_whitelist)
        print "List comparison's complete. Uploading changes to the Juniper"
        print "Length of final whitelist: " + str(len(final_whitelist))
        upload_whitelist_to_juniper(final_whitelist, juniper_whitelist)
    else:
        print "The Juniper whitelist is empty, uploading the Pentaho list to the Juniper"
        final_whitelist = final_whitelist
        upload_whitelist_to_juniper(final_whitelist, juniper_whitelist)
    print "Sending email out with the changes"
    #send_email()
    print "putting a date on the gateway_ips.csv file."
    local('mv <NEW> <OLD.%s>' % TODAY)
