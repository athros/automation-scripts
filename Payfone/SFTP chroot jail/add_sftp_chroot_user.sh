#!/bin/sh

#Script based on the article located at: http://www.sanitarium.net/unix_stuff/sftp_chroot.html

USERNAME=$1
GROUP="sftponly"
PASSWORD=`openssl rand -base64 12`
CHROOT_ROOT_PATH="/sftp/${USERNAME}"

#Create the user with the correct parameters
useradd --comment "SFTP chroot account" --home-dir /home/$USERNAME --group $GROUP --create-home --shell /sbin/nologin $USERNAME
#Add the password to the user without echoing it to the command line
echo $PASSWORD | passwd $USERNAME --stdin
#Create the chroot jail
mkdir -p $CHROOT_ROOT_PATH/home/$USERNAME
#Change the permissions and owners
chown -R root:sftponly $CHROOT_ROOT_PATH
chmod -R 750 $CHROOT_ROOT_PATH
#Build the etc directory needed by the chroot jail
mkdir $CHROOT_ROOT_PATH/etc
chgrp $GROUP $CHROOT_ROOT_PATH/etc
chmod 710 $CHROOT_ROOT_PATH/etc
#Create the passwd file for the chroot jail and add the chroot user and a fake root account
getent passwd $USERNAME > $CHROOT_ROOT_PATH/etc/passwd
echo "root:x:0:0:SFTP chroot root:::" >> $CHROOT_ROOT_PATH/etc/passwd
chmod 644 $CHROOT_ROOT_PATH/etc/passwd
#Create a group file with the basic group in it
getent group $GROUP > $CHROOT_ROOT_PATH/etc/group
getent group $USERNAME >> $CHROOT_ROOT_PATH/etc/group
chmod 644 $CHROOT_ROOT_PATH/etc/group
#Create a cross filesystem mount point and mount it
echo "/home/${USERNAME} /sftp/${USERNAME}/home/${USERNAME} bind defaults,bind 0 0" >> /etc/fstab
mount -v $CHROOT_ROOT_PATH/home/$USERNAME
#Echo the Username and password for documentation purposes
echo "USERNAME: ${USERNAME}"
echo "PASSWORD: ${PASSWORD}"
echo "Creation complete!"

# Don't forget to add the following to the bottom of /etc/ssh/sshd_config
#
#Subsystem      sftp    internal-sftp
#KbdInteractiveAuthentication yes

#Match group sftponly
#ChrootDirectory /sftp/%u
#X11Forwarding no
#AllowTcpForwarding no
#ForceCommand internal-sftp

#Also don't forget to groupadd sftponly ! 
