# Compliance document generation. Uses XMLRPCLIB to hook into Confluence for various pieces, including:
# An up to date autostart list to use to compare to the chkconfig on each server
# Connects to a confluence page to build the server list
# Updating a page on confluence with the latest compliance information.

from fabric.api import sudo, run, env, quiet, execute
from fabric.decorators import runs_once
from datetime import datetime
from bs4 import BeautifulSoup
import xmlrpclib, re, difflib, smtplib, traceback, string
from email.mime.text import MIMEText

env.user = ""
env.password = ""


CONFLUENCE_URL = ""
CONFLUENCE_LOGIN = ""
CONFLUENCE_PASSWORD = ""
FULL_CONTENT = " "
AUTOSTART_LIST = {}

EMAIL_CONTENT = []

CENTOS5 = False
CENTOS6 = False

PRODUCTION_SERVERS = []
PRODUCTION_CONTENT = " "

NONPRODUCTION_SERVERS = []
NONPRODUCTION_CONTENT = " "

line_break = "\n------------------------------------------------------------------------------------------------------------------------------------------------------\n"

global_hosts = []

@runs_once
def host_list_generation():
    centos_pattern = re.compile('CentOS')
    oel_pattern = re.compile('OEL')
    prod_pattern = re.compile('Production')
    PAGE_ID = ""
    client = xmlrpclib.Server(CONFLUENCE_URL, verbose = 0)
    auth_token = client.confluence2.login(CONFLUENCE_LOGIN, CONFLUENCE_PASSWORD)
    page = client.confluence2.getPage(auth_token, PAGE_ID)
    soup = BeautifulSoup(page['content'])
    client.confluence2.logout(auth_token) 
    iteration = 0
    table_length = len(soup.find_all('tr'))
    while iteration <= (table_length-1):
        table_index = iteration
        if table_index == 0:
            pass
        else:
            try:
                centos_match = re.search(centos_pattern, str(soup.find_all('tr')[table_index].find_all('td')[2].string))
                oel_match = re.search(oel_pattern, str(soup.find_all('tr')[table_index].find_all('td')[2].string))
                prod_match = re.search(prod_pattern, str(soup.find_all('tr')[table_index].find_all('td')[4].string))
                if centos_match or oel_match:
                    global global_hosts
                    server_name = str(soup.find_all('tr')[table_index].find_all('td')[0].string) + ".example.com"
                    global_hosts.append(server_name)
                    if prod_match:
                        global PRODUCTION_SERVERS
                        PRODUCTION_SERVERS.append(server_name)
                    else:
                        global NONPRODUCTION_SERVERS
                        NONPRODUCTION_SERVERS.append(server_name)
                else:
                    pass
            except:
                print("Breaking out of the Host list generation loop")
                break
        iteration += 1
    global env
    env.hosts = global_hosts

@runs_once
def autostart_list_generation():
    PAGE_ID = ""
    client = xmlrpclib.Server(CONFLUENCE_URL, verbose = 0)
    auth_token = client.confluence2.login(CONFLUENCE_LOGIN, CONFLUENCE_PASSWORD)
    page = client.confluence2.getPage(auth_token, PAGE_ID)
    soup = BeautifulSoup(page['content'])
    client.confluence2.logout(auth_token) 
    iteration = 1
    table_length = len(soup.find_all('tr')) # Full length for all table members
    while iteration <= table_length:
        table_index = iteration - 1
        if table_index == 0:
            pass
        else:
            global AUTOSTART_LIST
            AUTOSTART_LIST[soup.find_all('tr')[table_index].td.string] = soup.find_all('tr')[table_index].find_all('td')[2].string
        iteration += 1

def os_version_collection():
    centos_pattern = re.compile('CentOS')
    centos5_pattern = re.compile('5')
    centos6_pattern = re.compile('6')
    global CENTOS5
    global CENTOS6
    with quiet():
        os_version_command = run('/bin/cat /etc/redhat-release').splitlines()
        os_version = os_version_command[len(os_version_command) - 1]
        os_version_split = os_version.split()
        centos_match = re.search(centos_pattern, os_version_split[2])
        centos5_match = re.search(centos5_pattern, os_version_split[2])
        centos6_match = re.search(centos6_pattern, os_version_split[2])
        if centos_match and centos5_match:
            CENTOS5 = True
            CENTOS6 = False
        elif centos_match and centos6_match:
            CENTOS6 = True
            CENTOS5 = False
        else:
            CENTOS5 = False
            CENTOS6 = False
        return os_version

def running_iptables_collection():
    with quiet():
        iptables_running_content_command = " "
        if CENTOS5:
            iptables_running_content_command = sudo('/sbin/iptables-save')
        elif CENTOS6:
            iptables_running_content_command = sudo('/sbin/iptables-save')[220:]
        else:
            iptables_running_content_command = sudo('/sbin/iptables-save')
        iptables_running_content = line_break + "IPTABLES in memory:" + line_break + iptables_running_content_command + line_break
        return iptables_running_content

def saved_iptables_collection():
    with quiet():
        iptables_saved_content_command = " "
        if CENTOS5:
            iptables_saved_content_command = sudo('/bin/cat /etc/sysconfig/iptables')
        elif CENTOS6:
            iptables_saved_content_command = sudo('/bin/cat /etc/sysconfig/iptables')[220:]
        else:
            iptables_saved_content_command = sudo('/bin/cat /etc/sysconfig/iptables')
        iptables_saved_content = line_break + "IPTABLES saved file under /etc/sysconfig/iptables:" + line_break + iptables_saved_content_command + line_break
        return iptables_saved_content

def iptables_diff_collection():
    comment_pattern = re.compile('#')
    iptables_running = sudo('/sbin/iptables-save')
    iptables_running = filter(None, iptables_running.splitlines())
    iptables_saved = sudo('/bin/cat /etc/sysconfig/iptables')
    iptables_saved = filter(None, iptables_saved.splitlines())
    iptables_saved_clean = []
    iptables_running_clean = []
    iptables_diff = []
    for line in iptables_running:
        comment_match = re.search(comment_pattern, line)
        if not comment_match:
            iptables_running_clean.append(line.lstrip().rstrip())
    for line in iptables_saved:
        comment_match = re.search(comment_pattern, line)
        if not comment_match:
            iptables_saved_clean.append(line.lstrip().rstrip())
    diff = difflib.unified_diff(iptables_running_clean, iptables_saved_clean, lineterm='', n=0)
    for line in diff:
        iptables_diff.append(line) 
    iptables_diff_final = line_break + "IPTABLES Differences between running configuration and saved configuration" + line_break + '\n'.join(iptables_diff) + line_break
    return iptables_diff_final

def autostart_collection():
    global EMAIL_CONTENT
    with quiet():
        autostart_pattern = re.compile(':on')
        xinetd_process_pattern = re.compile(":")
        disabled_pattern = re.compile("DISABLE")
        host = str(env.host_string).split('.')[0]
        host_pattern = re.compile(host)
        autostart_content = ""
        if CENTOS5:
            autostart_content = run('/sbin/chkconfig --list').splitlines()
        elif CENTOS6:
            autostart_content = run('/sbin/chkconfig --list')[220:].splitlines()
        else:
            autostart_content = run('/sbin/chkconfig --list').splitlines()
        autostart_lines = []
        autostart = []
        for line in autostart_content:
            auto_on_match = re.search(autostart_pattern, line)
            if auto_on_match:
                item = line.split()[0]
                xinetd_process_match = re.search(xinetd_process_pattern, item)
                if xinetd_process_match:
                    item = xinetd_process_pattern.sub( '', str(item))
                    autostart_lines.append(item)
                else:
                    autostart_lines.append(item)
        for item in autostart_lines:
            try: 
                line = AUTOSTART_LIST[item]
                disable_match = re.search(disabled_pattern, str(line))
                host_match = re.search(host_pattern, str(line))
                if len(item) > 14:
                    autostart.append(item + "\t:\t" +  str(line))
                    if disable_match and not host_match:
                        EMAIL_CONTENT.append(str(env.host_string) + " : " + item + "\t:\t" +  str(line))
                elif len(item) > 11 and len(item) <= 14:
                    autostart.append(item + "\t\t:\t" + str(line))
                    if disable_match and not host_match:
                        EMAIL_CONTENT.append(str(env.host_string) + " : " + item + "\t\t:\t" + str(line))
                elif len(item) >= 8 and len(item) <= 11:
                    autostart.append(item + "\t\t\t:\t" + str(line))
                    if disable_match and not host_match:
                        EMAIL_CONTENT.append(str(env.host_string) + " : " + item + "\t\t\t:\t" + str(line))
                elif len(item) < 4:
                    autostart.append(item + "\t\t\t\t\t:\t" + str(line))
                    if disable_match and not host_match:
                        EMAIL_CONTENT.append(str(env.host_string) + " : " + item + "\t\t\t\t\t:\t" + str(line))
                else:
                    autostart.append(item + "\t\t\t\t:\t" + str(line))
                    if disable_match and not host_match:
                        EMAIL_CONTENT.append(str(env.host_string) + " : " + item + "\t\t\t\t:\t" + str(line))
            except KeyError:
                if len(item) > 12:
                    autostart.append(item + "\t:\tUnknown!")
                    EMAIL_CONTENT.append("\n" + str(env.host_string) + " : " + item + "\t:\tUnknown!")
                elif len(item) > 10 and len(item) <= 12:
                    autostart.append(item + "\t\t:\tUnknown!")
                    EMAIL_CONTENT.append("\n" + str(env.host_string) + " : " + item + "\t\t:\tUnknown!")
                else:
                    autostart.append(item + "\t\t\t\t:\tUnknown!")
                    EMAIL_CONTENT.append("\n" + str(env.host_string) + " : " + item + "\t\t\t\t:\tUnknown!")
        autostart_content = line_break + "Daemon Autostart:" + line_break + '\n'.join(autostart) + line_break
        return autostart_content

def ipaddress_collection():
    with quiet():
        ipaddrs = ""
        eth_pattern = re.compile('eth')
        addr_pattern = re.compile('addr')
        inet_pattern = re.compile('inet')
        inet6_pattern = re.compile('inet6')
        localhost_pattern = re.compile('127.0.0.1')
        ifconfig = run('/sbin/ifconfig')
        ifconfig = ifconfig.splitlines()
        for line in ifconfig:
            eth_match = re.search(eth_pattern, line)
            addr_match = re.search(addr_pattern, line)
            inet_match = re.search(inet_pattern, line)
            inet6_match = re.search(inet6_pattern, line)
            localhost_match = re.search(localhost_pattern, line)
            if eth_match:
                #print line
                ipaddrs = ipaddrs + line.split()[0] + " : "
            if addr_match and inet_match and not inet6_match:
                line_split = line.split()
                addr = line_split[1].split(':')[1]
                if not localhost_match:
                    #print line
                    ipaddrs = ipaddrs + addr + " :: "
        return ipaddrs


def tripwire_collection():
    comment_pattern = re.compile('#')
    with quiet():
        version_pattern = re.compile('version:')
        identifier_pattern = re.compile('identifier:')
        tw_hosts_pattern = re.compile('hosts:')
        tripwire_information = ""
        if CENTOS5:
            tripwire_information = sudo('/bin/cat /etc/tripwire/twpol.txt')
        elif CENTOS6:
            tripwire_information = sudo('/bin/cat /etc/tripwire/twpol.txt')[220:]
        else:
            tripwire_information = sudo('/bin/cat /etc/tripwire/twpol.txt')
        tripwire_information = tripwire_information.splitlines()
        tripwire_unformatted = []
        for line in tripwire_information:
            comment_match = re.search(comment_pattern, line)
            version_match = re.search(version_pattern, line)
            identifier_match = re.search(identifier_pattern, line)
            tw_hosts_match = re.search(tw_hosts_pattern, line)
            if version_match:
                tripwire_unformatted.append(line)
            elif identifier_match:
                tripwire_unformatted.append(line)
            elif tw_hosts_match:
                tripwire_unformatted.append(line)
            elif not comment_match:
                tripwire_unformatted.append(line)
        tripwire_unformatted = filter(None, tripwire_unformatted)
        tripwire_content = line_break + "Tripwire Information:" + line_break + "\n".join(tripwire_unformatted) + line_break
        return tripwire_content

def likewise_collector():
    likewise_content = []
    likewise_unformatted = sudo('/opt/likewise/bin/lwconfig --show RequireMembershipOf')
    likewise_formatted = likewise_unformatted.splitlines()
    likewise_content = line_break + "Likewise Login Information" + line_break + "\n".join(likewise_formatted) + line_break
    return likewise_content

def sudoers_collector():
    version_pattern = re.compile('Version')
    template_pattern = re.compile('Template')
    comment_pattern = re.compile('#')
    if CENTOS5:
        sudoers_information = sudo('/bin/cat /etc/sudoers')
    elif CENTOS6:
        sudoers_information = sudo('/bin/cat /etc/sudoers')[220:]
    else:
        sudoers_information = sudo('/bin/cat /etc/sudoers')
    sudoers_unformatted = []
    sudoers_information = sudoers_information.splitlines()
    for line in sudoers_information:
        version_match = re.search(version_pattern, line)
        template_match = re.search(template_pattern, line)
        comment_match = re.search(comment_pattern, line)
        if version_match:
            sudoers_unformatted.append(line)
        elif template_match:
            sudoers_unformatted.append(line)
        elif not comment_match:
            sudoers_unformatted.append(line) 
    sudoers_unformatted = filter(None, sudoers_unformatted)
    sudoers_content = line_break + "Sudoers Information:" + line_break + "\n".join(sudoers_unformatted) + line_break
    return sudoers_content
	
def crontab_collector():
    sudo('chmod -R 777 /tmp')
    sudo('/opt/scripts/DisplayAllCronJobs.sh')
    cron_file = sudo('cat /tmp/cdisplay')
    cron_file = cron_file.splitlines()
    cron_file_content = []
    for line in cron_file:
        cron_file_content.append(line)
    cron_content = line_break + "CRONTAB Information:" + line_break + "\n".join(cron_file_content) + line_break
    return cron_content
    
def ntp_collection():
    comment_pattern = re.compile('#')
    star_pattern = re.compile('\*')
    root_motd_pattern = re.compile('Please try')
    ntp_file = sudo('/bin/cat /etc/ntp.conf')
    ntp_file_split = ntp_file.splitlines()
    ntp_content_list = []
    for line in ntp_file_split:
        comment_match = re.search(comment_pattern, line)
        star_match = re.search(star_pattern, line)
        root_motd_match = re.search(root_motd_pattern, line)
        if not comment_match and not star_match and not root_motd_match:
            ntp_content_list.append(line)
    ntp_content = line_break + "NTP Information: " + line_break + "\n".join(ntp_content_list) + line_break
    return ntp_content

def installed_packages_collection():
    comment_pattern = re.compile('#')
    star_pattern = re.compile('\*')
    root_motd_pattern = re.compile('Please try')
    load_pattern = re.compile('Load')
    install_pattern = re.compile('Install')
    base_pattern = re.compile('base')
    bogus_1 = re.compile('\[1m')
    bogus_2 = re.compile('\[m')
    package_list = sudo('/usr/bin/yum list installed')
    package_list = package_list.splitlines()
    package_list_split = []
    for line in package_list:
        line = line.lstrip().rstrip()
        comment_match = re.search(comment_pattern, line)
        star_match = re.search(star_pattern, line)
        root_motd_match = re.search(root_motd_pattern, line)
        load_match = re.search(load_pattern, line)
        install_match = re.search(install_pattern, line)
        base_match = re.search(base_pattern, line)
        if not comment_match and not star_match and not root_motd_match and not load_match and not install_match and not base_match:
            newline = "".join(s for s in line if s in string.printable)
            newline = bogus_1.sub(r'', newline)
            newline = bogus_2.sub(r'', newline)
            package_list_split.append(newline)
    package_list_content = line_break + "Installed Packages and version numbers:" + line_break + "\n".join(package_list_split) + line_break
    return package_list_content

def collect_information():
    with quiet():
        os_version = os_version_collection()
        iptables_running_content = running_iptables_collection()
        iptables_saved_content = saved_iptables_collection()
        iptables_diff_content = iptables_diff_collection()
        autostart_content = autostart_collection()
        ipaddr = ipaddress_collection()
        tripwire_content = tripwire_collection()
        likewise_content = likewise_collector()
        sudoers_content = sudoers_collector()
        cron_content = crontab_collector()
        ntp_content = ntp_collection()
        package_list_content = installed_packages_collection()
        box_title = env.host_string + " | " + ipaddr + " | " + os_version
        full_content = "\n" + iptables_running_content + "\n" + iptables_saved_content + "\n" + iptables_diff_content + "\n" + autostart_content + "\n" + likewise_content + "\n" + sudoers_content + "\n" + cron_content + "\n" + ntp_content + "\n" + package_list_content + "\n" + tripwire_content
        post_content = '<ac:structured-macro ac:name="code"><ac:parameter ac:name="title">%s</ac:parameter><ac:parameter ac:name="collapse">true</ac:parameter><ac:plain-text-body><![CDATA[%s]]></ac:plain-text-body></ac:structured-macro>' % (box_title, full_content)
        if env.host_string in PRODUCTION_SERVERS:
            global PRODUCTION_CONTENT
            PRODUCTION_CONTENT += "\n" + post_content
        elif env.host_string in NONPRODUCTION_SERVERS:
            global NONPRODUCTION_CONTENT
            NONPRODUCTION_CONTENT += "\n" + post_content
        else:
            print "ERROR Determining PRODUCTION or NONPRODUCTION"

@runs_once
def page_post():
    global FULL_CONTENT
    FULL_CONTENT = '\n\n<h1>Production Servers</h1>\n\n' + PRODUCTION_CONTENT + "\n" + line_break + '\n\n<h1>Non-Production Servers</h1>\n\n' + NONPRODUCTION_CONTENT
    TITLE_NOW = datetime.now()
    PAGE_ID = ""
    client = xmlrpclib.Server(CONFLUENCE_URL, verbose = 0)
    auth_token = client.confluence2.login(CONFLUENCE_LOGIN, CONFLUENCE_PASSWORD)
    page = client.confluence2.getPage(auth_token, PAGE_ID)
    page['title'] = "Server Compliance Documentation - Updated: " + str(TITLE_NOW)
    page['content'] = FULL_CONTENT
    #print page
    try:
        result = client.confluence2.storePage(auth_token, page)
        print result
    except Exception as e:
        print "Exception occurred! %s " % e
        traceback.print_exc()
    client.confluence2.logout(auth_token)

@runs_once
def send_email():
    from_addr = 'techops@payfone.com'
    to_addr = ['techops_updater@payfone.com']
    msg = "\n".join(EMAIL_CONTENT)
    message = MIMEText(msg)
    message['Subject'] = "Server Compliance Page Alerts on " + str(datetime.now())
    message['From'] = from_addr
    message['To'] = ",".join(to_addr)
    server = smtplib.SMTP('<MAIL_SERVER>:25')
    server.sendmail(from_addr, to_addr, message.as_string())
    server.quit()

@runs_once
def update():
    print "Generating Host List"
    host_list_generation()
    print "Host list generated, generating the autostart lists."
    autostart_list_generation()
    print "Autostart list generated, proceeding to collection."
    execute(collect_information)
    print "Collection Complete. Posting to the page."
    page_post()
    print "Page posted, sending email."
    #send_email()
    print "Email sent. Work complete."
