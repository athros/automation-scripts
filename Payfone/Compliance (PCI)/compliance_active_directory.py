import active_directory # Module from http://timgolden.me.uk/python/active_directory.html
from datetime import datetime
import xmlrpclib, re, smtplib
from email.mime.text import MIMEText

CONFLUENCE_URL = ""
CONFLUENCE_LOGIN = ""
CONFLUENCE_PASSWORD = ""
USER_PAGE_ID = ''
GROUP_PAGE_ID = ''

#FULL_CONTENT = ""

#client = xmlrpclib.Server(CONFLUENCE_URL, verbose = 0)
#auth_token = client.confluence2.login(CONFLUENCE_LOGIN, CONFLUENCE_PASSWORD)
#page = client.confluence2.getPage(auth_token, PAGE_ID)

def get_user_list():
    """Search Active Directory for all users and return a dict of "user" : [group list]"""
    base_user_list = active_directory.search ("objectCategory='Person'", "objectClass='User'")
    user_list = []
    for user in base_user_list:
        if len(user.memberOf) == 0:
            pass
        elif not user.accountDisabled:
            user_list.append(user)
    user_dict = {}
    for user in user_list:
        group_list = []
        for group in user.memberOf:
            group_name = active_directory.find_group(group.cn)
            group_list.append(group_name)
        user_dict[user.cn] = group_list
    return user_dict

def user_collection_preperation(user_dictionary):
    """Format the data from the user dictionary for posting to a Confluence Page"""
    users_sorted = sorted(user_dictionary)
    for user in users_sorted:
        user_title = str(user)
        user_content_1 = []
        user_content_1.append("Group Name\t\tDescription\n---------------------------------\n")
        for group in user_dictionary[user]:
            g_description = active_directory.find_group(group.cn).description
            content_string = str(group.cn) + "\t\t" + str(g_description) + "\n"
            user_content_1.append(content_string)
        user_content = "\n".join(user_content_1)
        post_content = '<ac:structured-macro ac:name="code"><ac:parameter ac:name="title">%s</ac:parameter><ac:parameter ac:name="collapse">true</ac:parameter><ac:plain-text-body><![CDATA[%s]]></ac:plain-text-body></ac:structured-macro>' % (user_title, user_content)
        global FULL_CONTENT
        FULL_CONTENT += "\n" + post_content

def get_group_list():
    """Get a list of groups and push it into a dict of "group.cn" : [Users in group]"""
    group_dictionary = {}
    admin_pattern_1 = re.compile('Admin')
    admin_pattern_2 = re.compile('admin')
    prod_pattern = re.compile('PRD')
    apex_pattern_1 = re.compile('Apex')
    apex_pattern_2 = re.compile('apex')
    operator_pattern = re.compile('Operators')
    vpn_pattern = re.compile('VPN')
    access_pattern = re.compile('Access')
    kvm_pattern = re.compile('KVM')
    group_list = ['Remote Desktop Users']
    for group in active_directory.search (objectClass='group'):
        admin_1_match = re.search(admin_pattern_1, str(group.cn))
        admin_2_match = re.search(admin_pattern_2, str(group.cn))
        prod_match = re.search(prod_pattern, str(group.cn))
        apex_1_match = re.search(apex_pattern_1, str(group.cn))
        apex_2_match = re.search(apex_pattern_2, str(group.cn))
        operator_match = re.search(operator_pattern, str(group.cn))
        vpn_match = re.search(vpn_pattern, str(group.cn))
        access_match = re.search(access_pattern, str(group.cn))
        kvm_match = re.search(kvm_pattern, str(group.cn))
        if admin_1_match or admin_2_match or prod_match or apex_1_match or apex_2_match or operator_match or vpn_match or access_match or kvm_match:
            group_list.append(str(group.cn))
    for group in group_list:
        user_list = []
        group_search = active_directory.find_group(group)
        for user in group_search.member:
            user_list.append(str(user.cn))
        group_dictionary[group] = sorted(user_list)
    return group_dictionary

def group_collection_preperation(group_dictionary):
    """Format the data from the group dictionary for posting to a Confluence Page"""
    group_dictionary_keys_sorted = sorted(group_dictionary)
    for group in group_dictionary_keys_sorted:
        group_title = group
        group_content_1 = []
        group_content_1.append("Group Description: " + str(active_directory.find_group(group).description) + "\n")
        if len(group_dictionary[group]) > 0:
            for user in group_dictionary[group]:
                group_content_1.append(user)
        else:
            group_content_1.append("No users in this group!")
        group_content = "\n".join(group_content_1)
        post_content = '<ac:structured-macro ac:name="code"><ac:parameter ac:name="title">%s</ac:parameter><ac:parameter ac:name="collapse">true</ac:parameter><ac:plain-text-body><![CDATA[%s]]></ac:plain-text-body></ac:structured-macro>' % (group_title, group_content)
        global FULL_CONTENT
        FULL_CONTENT += "\n" + post_content

def page_post(page_to_update):
        """Post to the Confluence page based on what PAGE_ID is passed in"""
        client = xmlrpclib.Server(CONFLUENCE_URL, verbose = 0)
        auth_token = client.confluence2.login(CONFLUENCE_LOGIN, CONFLUENCE_PASSWORD)
        page = client.confluence2.getPage(auth_token, page_to_update)
        if page_to_update == USER_PAGE_ID:
            page['title'] = "Active Directory User List"
        elif page_to_update == GROUP_PAGE_ID:
            page['title'] = "Active Directory Group List"
        page['content'] = FULL_CONTENT
        result = client.confluence2.storePage(auth_token, page)
        client.confluence2.logout(auth_token)

def send_email():
    from_addr = 'techops@payfone.com'
    to_addr = ['techops_updater@payfone.com']
    msg = "Greetings,\n\nThe Active Driectory Users and Groups have been updated, and require approvals!\n\nThanks,\n\n- The Operations team at Payfone"
    message = MIMEText(msg)
    message['Subject'] = "Active Directory Users and Groups updated on " + str(datetime.now())
    message['From'] = from_addr
    message['To'] = ",".join(to_addr)
    server = smtplib.SMTP('')
    server.sendmail(from_addr, to_addr, message.as_string())
    server.quit()

if __name__ == "__main__":
    global FULL_CONTENT
    FULL_CONTENT = "Updated: " + str(datetime.now()) + "\n"
    print "Getting User List"
    users = get_user_list()
    print "User List collected, formatting the data"
    user_collection_preperation(users)
    print "User data formatted, posting to the confluence page"
    page_post(USER_PAGE_ID)
    print "User page post complete, resetting FULL_CONTENT"
    FULL_CONTENT = "Updated: " + str(datetime.now()) + "\n"
    print "FULL_CONTENT reset, getting the group list"
    groups = get_group_list()
    print "Group list collected, preparing the data for posting"
    group_collection_preperation(groups)
    print "Data preperation complete, posting to the group page"
    page_post(GROUP_PAGE_ID)
    print "Group page post complete. Sending email out for ticket creation"
    send_email()
    print "Script complete!"
