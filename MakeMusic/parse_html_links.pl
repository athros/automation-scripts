#!/usr/local/Cellar/perl/5.22.1/bin/perl

use strict;
use warnings;

use HTML::Parser;
use File::Find;
use Path::Tiny qw(path);

my $data = "";

my $find_links = HTML::Parser->new(
    start_h => [
        sub {
            my ($tag, $attr) = @_;
            if ($tag eq 'link' and exists $attr->{href}) {
                my $orig_data = $attr->{href};
                my $lc_data = lc $attr->{href};
                $data =~ s/$orig_data/$lc_data/g
            }
            elsif ($tag eq 'script' and exists $attr->{src}) {
                my $orig_data = $attr->{src};
                my $lc_data = lc $attr->{src};
                $data =~ s/$orig_data/$lc_data/g
            }
            elsif ($tag eq 'img' and exists $attr->{src}) {
                my $orig_data = $attr->{src};
                my $lc_data = lc $attr->{src};
                $data =~ s/$orig_data/$lc_data/g
            }
        }, 
        "tag, attr"
    ]
);

find( \&wanted, shift );

sub wanted {
    return unless -f $_ && m/htm$/;
    print "FILENAME: $_\n:";
    my $file = path($_);
    $data = $file->slurp;
    $find_links->parse($data);
    $file->spew($data);
}

