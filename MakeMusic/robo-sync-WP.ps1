## This Script Mirrors a directory tree from source to destination with the Windows builtin command robocopy.
## Exit codes from robocopy are logged to Windows Eventlog.
## Author: NIKLAS JUMLIN

## Updated by Nick Sawlidi for MakeMusic/peaksware - 14APR15
## WordPress site sync (Plugins et. al.)
 
## Usage: Run with administrative rights in Windows Task Scheduler or Administrator:PowerShell
## If not executed with administrative privileges the script will not write to eventlog.
 
## Change these parameters
## ================================================================

##Folder arrays for sync. Item 1 is always the source, item 2 is the destination

#SmartMusic
$sm_upload = "C:\inetpub\wordpress-sites\smartmusic.com\wp-content\uploads\", "\\webserver2\c$\inetpub\wordpress-sites\smartmusic.com\wp-content\uploads\"
$sm_promo = "C:\hosting\www\SmartMusic\promotions\", "\\webserver2\c$\hosting\www\SmartMusic\promotions\"
$sm_plugins = "C:\inetpub\wordpress-sites\smartmusic.com\wp-content\plugins\", "\\webserver2\c$\inetpub\wordpress-sites\smartmusic.com\wp-content\plugins\"

#MusicXML

$mx_upload = "C:\inetpub\wordpress-sites\musicxml.com\wp-content\uploads\", "\\webserver2\c$\inetpub\wordpress-sites\musicxml.com\wp-content\uploads\"
$mx_plugins = "C:\inetpub\wordpress-sites\musicxml.com\wp-content\plugins\", "\\webserver2\c$\inetpub\wordpress-sites\musicxml.com\wp-content\plugins\"

#MakeMusic

$mm_upload = "C:\inetpub\wordpress-sites\makemusic.com\wp-content\uploads\", "\\webserver2\c$\inetpub\wordpress-sites\makemusic.com\wp-content\uploads\"
$mm_plugins = "C:\inetpub\wordpress-sites\makemusic.com\wp-content\plugins\", "\\webserver2\c$\inetpub\wordpress-sites\makemusic.com\wp-content\plugins\"

#Garritan

$g_upload = "C:\inetpub\wordpress-sites\garritan.com\wp-content\uploads\", "\\webserver2\c$\inetpub\wordpress-sites\garritan.com\wp-content\uploads\"
$g_promo = "C:\hosting\www\Garritan\promotions\", "\\webserver2\c$\hosting\www\Garritan\promotions\"
$g_plugins = "C:\inetpub\wordpress-sites\garritan.com\wp-content\plugins\", "\\webserver2\c$\inetpub\wordpress-sites\garritan.com\wp-content\plugins\"

#FinaleMusic

$fm_promo = "C:\hosting\www\FinaleMusic\promotions\", "\\webserver2\c$\hosting\www\FinaleMusic\promotions\"
$fm_stg_upload = "C:\inetpub\wordpress-sites\finalemusic.com\wp-content\uploads\", "\\stageweb5\c$\inetpub\wordpress-sites\staging.finalemusic.com\wp-content\uploads\"
$fm_upload = "C:\inetpub\wordpress-sites\finalemusic.com\wp-content\uploads\", "\\webserver2\c$\inetpub\wordpress-sites\finalemusic.com\wp-content\uploads\"
$fm_plugins = "C:\inetpub\wordpress-sites\finalemusic.com\wp-content\plugins\", "\\webserver2\c$\inetpub\wordpress-sites\finalemusic.com\wp-content\plugins\"

#Array of sites. Used in the FOREACH loop below.
$sites = $sm_plugins, $sm_promo, $sm_upload, $mx_plugins, $mx_upload, $mm_plugins, $mm_upload, $g_plugins, $g_promo, $g_upload, $fm_plugins, $fm_promo, $fm_stg_upload, $fm_upload

## Name of the job, name of source in Windows Event Log and name of robocopy Logfile.
$JOB = "RoboCopy-Script"

ForEach ($site in $sites) {

	## Source directory
	$SOURCE = ""
	 
	## Destination directory. Files in this directory will mirror the source directory. Extra files will be deleted!
	$DESTINATION = ""

    if ($site[0] -match '^C') {
        $SOURCE = $site[0]
        $DESTINATION = $site[1]
    }
    else {
        $SOURCE = $site[1]
        $DESTINATION = $site[0]
    }
	 
	## Path to robocopy logfile
	$LOGFILE = "C:\Users\nsawlidi\$JOB"
	 
	## Log events from the script to this location
	$SCRIPTLOG = "C:\Users\nsawlidi\$JOB-scriptlog.log"
	 
	## Mirror a directory tree
	$WHAT = @("/MIR")
	## /COPY:DATS: Copy Data, Attributes, Timestamps, Security
	## /SECFIX : FIX file SECurity on all files, even skipped files.
	 
	## Retry open files 3 times wait 30 seconds between tries. 
	$OPTIONS = @("/R:3","/W:30","/NDL","/NFL")
	## /NFL : No File List - don't log file names.
	## /NDL : No Directory List - don't log directory names.
	## /L   : List only - don't copy, timestamp or delete any files.
	 
	## This will create a timestamp like yyyy-mm-yy
	$TIMESTAMP = get-date -uformat "%Y-%m%-%d"
	 
	## This will get the time like HH:MM:SS
	$TIME = get-date -uformat "%T"
	 
	## Append to robocopy logfile with timestamp
	$ROBOCOPYLOG = "/LOG+:$LOGFILE`-Robocopy`-$TIMESTAMP.log"
	 
	## Wrap all above arguments
	$cmdArgs = @("$SOURCE","$DESTINATION",$WHAT,$ROBOCOPYLOG,$OPTIONS)
	 
	## ================================================================
	 
	## Start the robocopy with above parameters and log errors in Windows Eventlog.
	& C:\Windows\System32\Robocopy.exe @cmdArgs
	 
	## Get LastExitCode and store in variable
	$ExitCode = $LastExitCode
	 
	$MSGType=@{
	"16"="Error"
	"8"="Error"
	"4"="Warning"
	"2"="Information"
	"1"="Information"
	"0"="Information"
	}
	 
	## Message descriptions for each ExitCode.
	$MSG=@{
	"16"="Serious error. robocopy did not copy any files.`n
	Examine the output log: $LOGFILE`-Robocopy`-$TIMESTAMP.log"
	"8"="Some files or directories could not be copied (copy errors occurred and the retry limit was exceeded).`n
	Check these errors further: $LOGFILE`-Robocopy`-$TIMESTAMP.log"
	"4"="Some Mismatched files or directories were detected.`n
	Examine the output log: $LOGFILE`-Robocopy`-$TIMESTAMP.log.`
	Housekeeping is probably necessary."
	"2"="Some Extra files or directories were detected and removed in $DESTINATION.`n
	Check the output log for details: $LOGFILE`-Robocopy`-$TIMESTAMP.log"
	"1"="New files from $SOURCE copied to $DESTINATION.`n
	Check the output log for details: $LOGFILE`-Robocopy`-$TIMESTAMP.log"
	"0"="$SOURCE and $DESTINATION in sync. No files copied.`n
	Check the output log for details: $LOGFILE`-Robocopy`-$TIMESTAMP.log"
	}
	 
	## Function to see if running with administrator privileges
	function Test-Administrator  
	{  
		$user = [Security.Principal.WindowsIdentity]::GetCurrent();
		(New-Object Security.Principal.WindowsPrincipal $user).IsInRole([Security.Principal.WindowsBuiltinRole]::Administrator)  
	}
	 
	## If running with administrator privileges
	If (Test-Administrator -eq $True) {
		"Has administrator privileges"
	 
		## Create EventLog Source if not already exists
		if ([System.Diagnostics.EventLog]::SourceExists("$JOB") -eq $false) {
		"Creating EventLog Source `"$JOB`""
		[System.Diagnostics.EventLog]::CreateEventSource("$JOB", "Application")
		}
	 
		## Write known ExitCodes to EventLog
		if ($MSG."$ExitCode" -gt $null) {
			Write-EventLog -LogName Application -Source $JOB -EventID $ExitCode -EntryType $MSGType."$ExitCode" -Message $MSG."$ExitCode"
		}
		## Write unknown ExitCodes to EventLog
		else {
			Write-EventLog -LogName Application -Source $JOB -EventID $ExitCode -EntryType Warning -Message "Unknown ExitCode. EventID equals ExitCode"
		}
	}
	## If not running with administrator privileges
	else {
		## Write to screen and logfile
		Add-content $SCRIPTLOG "$TIMESTAMP $TIME No administrator privileges" -PassThru
		Add-content $SCRIPTLOG "$TIMESTAMP $TIME Cannot write to EventLog" -PassThru
	 
		## Write known ExitCodes to screen and logfile
		if ($MSG."$ExitCode" -gt $null) {
			Add-content $SCRIPTLOG "$TIMESTAMP $TIME Printing message to logfile:" -PassThru
			Add-content $SCRIPTLOG ($TIMESTAMP + ' ' + $TIME + ' ' + $MSG."$ExitCode") -PassThru
			Add-content $SCRIPTLOG "$TIMESTAMP $TIME ExitCode`=$ExitCode" -PassThru
		}
		## Write unknown ExitCodes to screen and logfile
		else {
			Add-content $SCRIPTLOG "$TIMESTAMP $TIME ExitCode`=$ExitCode (UNKNOWN)" -PassThru
		}
		Add-content $SCRIPTLOG ""
		Return
		}
}
