#!/usr/bin/env python3

''' This script is designed combine the Company spreadsheets, once exported as
    CSV together into one final CSV file. Requires Python 3, no extra modules/
    pip packages needed.'''

import csv
import argparse
import os.path
from glob import glob

# Command Line arguments
parser = argparse.ArgumentParser()
parser.add_argument("-srcpath", dest="srcpath", default=".", help="Path where the CSV files are located")
parser.add_argument("-destpath", dest="destpath", default=".", help="Path where the output file will be placed")
parser.add_argument("-output", dest="output_filename", default="finalcsv.csv", help="Filename for the output file")
args = parser.parse_args()

# Hyperlink trings for fields Royalty Link and company.com Link
royalty_link_prefix = "http://royalties.company.com/%28S%285pr2ng55u0nx4h45125pv4ji%29%29/Product_Detail.aspx?Product="
company_com_link_prefix = "http://www.company.com/Products/"
company_com_link_postfix = ".aspx"

# Put the destpath and output together to get a final output_file for csv.writer
output_file = os.path.join(args.destpath, args.output_filename)

# Put the srcpath together with *.csv for glob
filelist = glob(os.path.join(args.srcpath, "*.csv"))

# Fieldnames are from the sample CSV files given to me by Heath
fieldnames = ["Related Physical SKU",
                "Songcode1",
                "Subcode1",
                "Songcode2",
                "Subcode2",
                "Songcode3",
                "Subcode3",
                "Songcode4",
                "Subcode4",
                "Songcode5",
                "Subcode5",
                "Royalty Link",
                "company.com Link",
                "Product Title",
                "Song Title",
                "Arrangement Type",
                "Need to Send to",
                "Date Due",
                "PDF Needed?",
                "Finale Needed?",
                "Sibelius Needed?",
                "Audio Needed?",
                "XML Needed?",
                "CoverArt Files Needed?",
                "Territory",
                "company's Claim",
                "Copyright",
                "TSM Candidate?",
                "Request Notes",
                "James' Catalog Identifier",
                "Finale File Status",
                "PDF File Status",
                "Audio File Status",
                "Cover Status",
                "Servers Checked",
                "Digital Press?",
                "Notes",
                "Physical Status"]


#Check the filelist - if it's blank, kill the script.
if not filelist:
    print("No files at the location - " + str(args.srcpath) + " - Please input the full path with the files.")
    exit(1)

# Open each file sequentially and merge into one file.
with open(output_file, 'w') as endfile:
    writer = csv.DictWriter(endfile, fieldnames=fieldnames)
    writer.writeheader()
    for file in filelist:
        with open(file) as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                if str(row['Related Physical SKU']) != '':
                    royalty_link_data = row['Royalty Link']
                    row['Royalty Link'] = royalty_link_prefix + str(royalty_link_data)
                    company_com_link_data = row['Company.com Link']
                    row['Company.com Link'] = company_com_link_prefix + str(company_com_link_data) + company_com_link_postfix
                    writer.writerow(row)
