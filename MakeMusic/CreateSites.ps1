﻿param([string]$Name = "Unknown",
      [string]$Environment = "" )

Import-Module WebAdministration

#{{{ FUNCTION Create Directory Structure
function CreateDirectoryStructure
{
    param([string]$DirName = "Unknown",
          [string]$ApplicationRoot = "Unknown",
          [string]$LogPath = "Unknown",
		  [string]$WebDataDir = "Unknown")

    if (Test-Path D:\sites -pathType Container)
    {
        New-Item D:\sites\site\$DirName -type Directory
        New-Item D:\sites\log\$DirName -type Directory
		New-Item D:\sites\webdata\$DirName -type Directory
    }
    else
    {
        New-Item D:\sites -type Directory
        New-Item D:\sites\site\$DirName -type Directory
        New-Item D:\sites\log\$DirName -type Directory
		New-Item D:\sites\webdata\$DirName -type Directory
    }

    #Permissioning on the folders. Starting with the Application Root (D:\sites\site\$Name)
    $inherit = [system.security.accesscontrol.InheritanceFlags]"ContainerInherit, ObjectInherit"
    $propagation = [system.security.accesscontrol.PropagationFlags]"None"
    $acl = Get-Acl $ApplicationRoot                                                                 # Get Current acl
    $accessRule = New-Object System.Security.AccessControl.FileSystemAccessRule("NetworkService", "ReadAndExecute", $inherit, $propagation, "Allow")
    $acl.AddAccessRule($accessRule)                                                                 # Add rule for Network Service
    $accessRule = New-Object System.Security.AccessControl.FileSystemAccessRule("Everyone", "ReadAndExecute", $inherit, $propagation, "Allow")
    $acl.AddAccessRule($accessRule)                                                                 # Add Rule for IIS Account
    $accessRule = New-Object System.Security.AccessControl.FileSystemAccessRule("CODA\Domain Admins", "FullControl", $inherit, $propagation, "Allow")
    $acl.AddAccessRule($accessRule)
    Set-Acl -AclObject $acl $ApplicationRoot                                                        # Apply Changes

    #Log storage directory permissions (D:\sites\log\$Name)
    $acl = Get-Acl $LogPath                                                                         # Get Current acl
    $accessRule = New-Object System.Security.AccessControl.FileSystemAccessRule("NetworkService", "Read", $inherit, $propagation, "Allow")
    $acl.AddAccessRule($accessRule)
    $accessRule = New-Object System.Security.AccessControl.FileSystemAccessRule("NetworkService", "Write", $inherit, $propagation, "Allow")
    $acl.AddAccessRule($accessRule)                                                                 # Add rules for Network Service
    $accessRule = New-Object System.Security.AccessControl.FileSystemAccessRule("Everyone", "Read", $inherit, $propagation, "Allow")
    $acl.AddAccessRule($accessRule)
    $accessRule = New-Object System.Security.AccessControl.FileSystemAccessRule("Everyone", "Write", $inherit, $propagation, "Allow")
    $acl.AddAccessRule($accessRule)                                                                 # Add Rules for IIS Account
    $accessRule = New-Object System.Security.AccessControl.FileSystemAccessRule("CODA\Domain Admins", "FullControl", $inherit, $propagation, "Allow")
    $acl.AddAccessRule($accessRule)
    Set-Acl -AclObject $acl $LogPath

	#WebData directory permissions (D:\sites\webdata\$Name)
	$acl = Get-Acl $WebDataDir                                                                         # Get Current acl
    $accessRule = New-Object System.Security.AccessControl.FileSystemAccessRule("NetworkService", "Read", $inherit, $propagation, "Allow")
    $acl.AddAccessRule($accessRule)
    $accessRule = New-Object System.Security.AccessControl.FileSystemAccessRule("NetworkService", "Write", $inherit, $propagation, "Allow")
    $acl.AddAccessRule($accessRule)                                                                 # Add rules for Network Service
    $accessRule = New-Object System.Security.AccessControl.FileSystemAccessRule("Everyone", "Read", $inherit, $propagation, "Allow")
    $acl.AddAccessRule($accessRule)
    $accessRule = New-Object System.Security.AccessControl.FileSystemAccessRule("Everyone", "Write", $inherit, $propagation, "Allow")
    $acl.AddAccessRule($accessRule)                                                                 # Add Rules for IIS Account
    $accessRule = New-Object System.Security.AccessControl.FileSystemAccessRule("CODA\Domain Admins", "FullControl", $inherit, $propagation, "Allow")
    $acl.AddAccessRule($accessRule)
    Set-Acl -AclObject $acl $LogPath
}
#}}}

#{{{ FUNCTION Create App Pool
function CreateAppPool
{
    param([string]$APName = "Unknown",
          [int]$managedPipelineMode = 0,
          [string]$managedRuntimeVersion = "0")
    if (!(Test-Path IIS:\AppPools\$APName))
    {
        New-Item IIS:\AppPools\$APName
        Set-ItemProperty IIS:\AppPools\$APName managedPipelineMode $managedPipelineMode
        Set-ItemProperty IIS:\AppPools\$APName managedRuntimeVersion $managedRuntimeVersion
    }
}
#}}}

#{{{ FUNCTION Create the Site. Pull the certificate if needed. Build the Bindings. Hack the Planet!
function CreateSite
{
    param([string]$SiteName = "Unknown",
          [string]$DomainName = "TEST",
          [string]$Environment = "TEST",
          [string]$ApplicationRoot = "Unknown",
          [string]$AppPoolName = "Unknown")

    $FullSiteName = "$Environment$SiteName.$DomainName.com"

    #Certificate install. If it's not already imported, import it. Otherwise, skip
    if ($DomainName -eq "smartmusic")
    {
        $cert_exists = Get-ChildItem -Path cert:\LocalMachine\My -DNSName "*smartmusic*"
        if (!$cert_exists)
        {
            Get-ChildItem -Path "\\mmfile\Business Systems\IT\Certs\Certificates\smartmusic\wildcard.smartmusic.com.pfx" | Import-PfxCertificate -CertStoreLocation Cert:\localMachine\My –Exportable
        }

    }
    elseif ($DomainName -eq "makemusic")
    {
        $cert_exists = Get-ChildItem -Path cert:\LocalMachine\My -DNSName "*makemusic*"
        if (!$cert_exists)
        {
            Get-ChildItem -Path "\\mmfile\Business Systems\IT\Certs\Certificates\makemusic\wildcard.makemusic.com.pfx" | Import-PfxCertificate -CertStoreLocation Cert:\localMachine\My –Exportable
        }
    }

    #Create the Site
    if(!(Test-Path IIS:\Sites\$FullSiteName))
    {
        #HTTP Binding
        $IPAddress = "*"
        $BindingInfo = "{0}:80:{1}" -replace "\{0\}", $IPAddress -replace "\{1\}", $FullSiteName
        New-Item IIS:\Sites\$FullSiteName -physicalPath $ApplicationRoot -bindings @{protocol="http"; bindingInformation=$BindingInfo}
        #Set-ItemProperty IIS:\Sites\$SiteName -Name ApplicationPool -Value $AppPoolName
        Set-ItemProperty IIS:\Sites\$FullSiteName -Name applicationPool -Value $Name

        #HTTPS Binding
        cd IIS:\sslBindings
        $bound = Get-ChildItem | Where {$_.Port -eq 443}
        if($bound.Port -ne 443)
        {
            $cert = Get-ChildItem cert:\LocalMachine\MY | Where-Object{$_.Subject.Contains("$DomainName.com")}
            $cert | New-Item 0.0.0.0!443
        }
        New-WebBinding -Port 443 -Name $FullSiteName -HostHeader $FullSiteName -IP $IPAddress -Protocol https
    }

}
#}}}

Write-Host "Building $Name"

switch($Name)
{
    "Unknown"
    {
        Write-Host "You must put a name with -Name <SITENAME>!"
        break
    }
	########################
	#BEGIN SMARTMUSIC SITES#
	########################
    "smartmusic"
    {
        $SmartMusicNames = "gradebook","sps","smcontent","components","gbm","configuration","services","content","redirect"
        $DomainName = "smartmusic"
        foreach ($n in $SmartMusicNames)
        {
            $Name = $n
            $ApplicationRoot = "D:\sites\site\$Name"
            $LogPath = "D:\sites\log\$Name"
			$WebDataPath = "D:\sites\webdata\$Name"
            $AppPoolName = "IIS:\AppPools\$Name"

            CreateDirectoryStructure -DirName $Name -ApplicationRoot $ApplicationRoot -LogPath $LogPath -WebData $WebDataPath
            CreateAppPool -APName $Name -managedPipelineMode 0 -managedRuntimeVersion "v4.0"
            CreateSite -SiteName $Name -DomainName $DomainName -Environment $Environment -ApplicationRoot $ApplicationRoot -AppPoolName $AppPoolName
        }
    }

    "updates"
    {
        $DomainName = "smartmusic"
        $ApplicationRoot = "D:\sites\site\$Name"
        $LogPath = "D:\sites\log\$Name"
        $AppPoolName = "IIS:\AppPools\$Name"
        $WebDataPath = "D:\sites\webdata\$Name"

        CreateDirectoryStructure -DirName $Name -ApplicationRoot $ApplicationRoot -LogPath $LogPath -WebData $WebDataPath
        CreateAppPool -APName $Name -managedPipelineMode 1 -managedRuntimeVersion "v4.0"
        CreateSite -SiteName $Name -DomainName $DomainName -Environment $Environment -ApplicationRoot $ApplicationRoot -AppPoolName $AppPoolName
    }
    "wizards"
    {
        $DomainName = "smartmusic"
        $ApplicationRoot = "D:\sites\site\$Name"
        $LogPath = "D:\sites\log\$Name"
        $AppPoolName = "IIS:\AppPools\$Name"
        $WebDataPath = "D:\sites\webdata\$Name"

        CreateDirectoryStructure -DirName $Name -ApplicationRoot $ApplicationRoot -LogPath $LogPath -WebData $WebDataPath
        CreateAppPool -APName $Name -managedPipelineMode 1 -managedRuntimeVersion "v2.0"
        CreateSite -SiteName $Name -DomainName $DomainName -Environment $Environment -ApplicationRoot $ApplicationRoot -AppPoolName $AppPoolName
    }
	"store.smartmusic.com"
    {
        $Name = "store"
        $DomainName = "smartmusic"
        $ApplicationRoot = "D:\sites\site\$Name"
        $LogPath = "D:\sites\log\$Name"
        $AppPoolName = "IIS:\AppPools\$Name"
        $WebDataPath = "D:\sites\webdata\$Name"

        CreateDirectoryStructure -DirName $Name -ApplicationRoot $ApplicationRoot -LogPath $LogPath -WebData $WebDataPath
        CreateAppPool -APName $Name -managedPipelineMode 1 -managedRuntimeVersion "v4.0"
        CreateSite -SiteName $Name -DomainName $DomainName -Environment $Environment -ApplicationRoot $ApplicationRoot -AppPoolName $AppPoolName
    }
    "carousel"
    {
        $Name = "carousel"
        $DomainName = "smartmusic"
        $ApplicationRoot = "D:\sites\site\$Name"
        $LogPath = "D:\sites\log\$Name"
        $AppPoolName = "IIS:\AppPools\$Name"
        $WebDataPath = "D:\sites\webdata\$Name"

        CreateDirectoryStructure -DirName $Name -ApplicationRoot $ApplicationRoot -LogPath $LogPath -WebData $WebDataPath
        CreateAppPool -APName $Name -managedPipelineMode 1 -managedRuntimeVersion "v4.0"
        CreateSite -SiteName $Name -DomainName $DomainName -Environment $Environment -ApplicationRoot $ApplicationRoot -AppPoolName $AppPoolName
    }
	######################
	#END SMARTMUSIC SITES#
	######################
	#######################
	#BEGIN MAKEMUSIC SITES#
	#######################
	"makemusic_general"
    {
		$MakeMusicNames = "account","services","redirect"
		$DomainName = "makemusic"
		foreach ($n in $MakeMusicNames)
        {
            $Name = $n
            $ApplicationRoot = "D:\sites\site\$Name"
            $LogPath = "D:\sites\log\$Name"
			$WebDataPath = "D:\sites\webdata\$Name"
            $AppPoolName = "IIS:\AppPools\$Name"

            CreateDirectoryStructure -DirName $Name -ApplicationRoot $ApplicationRoot -LogPath $LogPath -WebData $WebDataPath
            CreateAppPool -APName $Name -managedPipelineMode 0 -managedRuntimeVersion "v4.0"
            CreateSite -SiteName $Name -DomainName $DomainName -Environment $Environment -ApplicationRoot $ApplicationRoot -AppPoolName $AppPoolName
        }
    }
	"account"
    {
        $DomainName = "makemusic"
        $ApplicationRoot = "D:\sites\site\$Name"
        $LogPath = "D:\sites\log\$Name"
        $AppPoolName = "IIS:\AppPools\$Name"

        CreateDirectoryStructure -DirName $Name -ApplicationRoot $ApplicationRoot -LogPath $LogPath
        CreateAppPool -APName $Name -managedPipelineMode 0 -managedRuntimeVersion "v4.0"
        CreateSite -SiteName $Name -DomainName $DomainName -Environment $Environment -ApplicationRoot $ApplicationRoot -AppPoolName $AppPoolName

    }
    "mmintranet"
    {
        $DomainName = "makemusic"
        $ApplicationRoot = "D:\sites\site\$Name"
        $LogPath = "D:\sites\log\$Name"
        $AppPoolName = "IIS:\AppPools\$Name"

        CreateDirectoryStructure -DirName $Name -ApplicationRoot $ApplicationRoot -LogPath $LogPath
        CreateAppPool -APName $Name -managedPipelineMode 1 -managedRuntimeVersion "v4.0"
        CreateSite -SiteName $Name -DomainName $DomainName -Environment $Environment -ApplicationRoot $ApplicationRoot -AppPoolName $AppPoolName

    }
    "dealers"
    {
        $DomainName = "makemusic"
        $ApplicationRoot = "D:\sites\site\$Name"
        $LogPath = "D:\sites\log\$Name"
        $AppPoolName = "IIS:\AppPools\$Name"

        CreateDirectoryStructure -DirName $Name -ApplicationRoot $ApplicationRoot -LogPath $LogPath
        CreateAppPool -APName $Name -managedPipelineMode 1 -managedRuntimeVersion "v4.0"
        CreateSite -SiteName $Name -DomainName $DomainName -Environment $Environment -ApplicationRoot $ApplicationRoot -AppPoolName $AppPoolName

    }
    "csmanual"
    {
        # MORE OPTIONS NEEDED FOR APP POOLS - Set-ItemProperty Shipping -name "enable32BitAppOnWin64" -Value "true"
        $DomainName = "makemusic"
        $ApplicationRoot = "D:\sites\site\$Name"
        $LogPath = "D:\sites\log\$Name"
        $AppPoolName = "IIS:\AppPools\$Name"

        CreateDirectoryStructure -DirName $Name -ApplicationRoot $ApplicationRoot -LogPath $LogPath
        CreateAppPool -APName $Name -managedPipelineMode 1 -managedRuntimeVersion "v4.0"
        CreateSite -SiteName $Name -DomainName $DomainName -Environment $Environment -ApplicationRoot $ApplicationRoot -AppPoolName $AppPoolName

    }
    "store"
    {
        $DomainName = "makemusic"
        $ApplicationRoot = "D:\sites\site\$Name"
        $LogPath = "D:\sites\log\$Name"
        $AppPoolName = "IIS:\AppPools\$Name"

        CreateDirectoryStructure -DirName $Name -ApplicationRoot $ApplicationRoot -LogPath $LogPath
        CreateAppPool -APName $Name -managedPipelineMode 1 -managedRuntimeVersion "v4.0"
        CreateSite -SiteName $Name -DomainName $DomainName -Environment $Environment -ApplicationRoot $ApplicationRoot -AppPoolName $AppPoolName

    }
    "services"
    {
        $DomainName = "makemusic"
        $ApplicationRoot = "D:\sites\site\$Name"
        $LogPath = "D:\sites\log\$Name"
        $AppPoolName = "IIS:\AppPools\$Name"

        CreateDirectoryStructure -DirName $Name -ApplicationRoot $ApplicationRoot -LogPath $LogPath
        CreateAppPool -APName $Name -managedPipelineMode 0 -managedRuntimeVersion "v4.0"
        CreateSite -SiteName $Name -DomainName $DomainName -Environment $Environment -ApplicationRoot $ApplicationRoot -AppPoolName $AppPoolName
    }
    "ws"
    {
        $Name = "ws"
        $DomainName = "makemusic"
        $ApplicationRoot = "D:\sites\site\$Name"
        $LogPath = "D:\sites\log\$Name"
        $AppPoolName = "IIS:\AppPools\$Name"

        CreateDirectoryStructure -DirName $Name -ApplicationRoot $ApplicationRoot -LogPath $LogPath
        CreateAppPool -APName $Name -managedPipelineMode 0 -managedRuntimeVersion "v2.0"
        CreateSite -SiteName $Name -DomainName $DomainName -Environment $Environment -ApplicationRoot $ApplicationRoot -AppPoolName $AppPoolName
        $WS_Names = "Authorization", "Gradebook", "SmartMusicService"
        foreach ($n in $WS_Names)
        {
            $Name = "WS-$n"
            $AppPoolName = "IIS:\AppPools\$Name"
            CreateAppPool -APName $Name -managedPipelineMode 0 -managedRuntimeVersion "v4.0"
        }
    }
    "redirect"
    {
        $DomainName = "makemusic"
        $ApplicationRoot = "D:\sites\site\$Name"
        $LogPath = "D:\sites\log\$Name"
        $AppPoolName = "IIS:\AppPools\$Name"
        $WebDataPath = "D:\sites\webdata\$Name"

        CreateDirectoryStructure -DirName $Name -ApplicationRoot $ApplicationRoot -LogPath $LogPath -WebData $WebDataPath
        CreateAppPool -APName $Name -managedPipelineMode 0 -managedRuntimeVersion "v4.0"
        CreateSite -SiteName $Name -DomainName $DomainName -Environment $Environment -ApplicationRoot $ApplicationRoot -AppPoolName $AppPoolName
    }
    "new_makemusic_deploy"
    {
		$MakeMusicNames = "authservices","orgservices","smartmusicadmin", "usermanagement", "emailservice", "schoolinfoservice"
		$DomainName = "makemusic"
		foreach ($n in $MakeMusicNames)
        {
            $Name = $n
            $ApplicationRoot = "D:\sites\site\$Name"
            $LogPath = "D:\sites\log\$Name"
			$WebDataPath = "D:\sites\webdata\$Name"
            $AppPoolName = "IIS:\AppPools\$Name"

            CreateDirectoryStructure -DirName $Name -ApplicationRoot $ApplicationRoot -LogPath $LogPath -WebData $WebDataPath
            CreateAppPool -APName $Name -managedPipelineMode 1 -managedRuntimeVersion "v4.0"
            CreateSite -SiteName $Name -DomainName $DomainName -Environment $Environment -ApplicationRoot $ApplicationRoot -AppPoolName $AppPoolName
        }
        $Name = "services"
        $DomainName = "smartmusic"
        $ApplicationRoot = "D:\sites\site\$Name"
        $LogPath = "D:\sites\log\$Name"
        $AppPoolName = "IIS:\AppPools\$Name"
        $WebDataPath = "D:\sites\webdata\$Name"

        CreateDirectoryStructure -DirName $Name -ApplicationRoot $ApplicationRoot -LogPath $LogPath -WebData $WebDataPath
        CreateAppPool -APName $Name -managedPipelineMode 1 -managedRuntimeVersion "v4.0"
        CreateSite -SiteName $Name -DomainName $DomainName -Environment $Environment -ApplicationRoot $ApplicationRoot -AppPoolName $AppPoolName
    }
}
